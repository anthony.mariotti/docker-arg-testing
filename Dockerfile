FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
FROM node:16.5-slim as node_base
ARG BUILD_VERSION=1.0.0

RUN echo $BUILD_VERSION

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

RUN echo "Completed"