﻿// See https://aka.ms/new-console-template for more information
using Serilog;
using System.Reflection;
using Utilities;

var logger = new LoggerConfiguration()
    .WriteTo.Console()
    .MinimumLevel.Information()
    .CreateLogger();

var version = Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion;

logger.Information(version?.TestingExtension());